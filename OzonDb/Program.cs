﻿using System.IO;
using System.IO.Compression;

namespace OzonDb {
    
    class Program {

        static void Main(string[] args)
        {
            var dbFileName = @"..\..\Data\OzonDb.sdf";
            using (var dbModule = new DataModule(string.Format("Data Source={0}", dbFileName)))
            {
                dbModule.Configuration.ValidateOnSaveEnabled = false;
                using (var zip = new ZipArchive(File.OpenRead(@"..\..\Data\div_book.zip"), ZipArchiveMode.Read, false)) 
                using (var stream = zip.GetEntry("div_book.xml").Open())
                using (var reader = new DataReader(stream))
                {
                        var dataMover = new DataMover(dbModule);
                        dataMover.LoadData(reader);
                }
            }
        }

    }
}
