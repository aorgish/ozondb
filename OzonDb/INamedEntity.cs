﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OzonDb {
    
    public interface INamedEntity {
        string Name { get; set; }
    }
}
