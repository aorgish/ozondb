﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

namespace OzonDb {
    
    public class DataMover {
        private IDictionary<string, Category>   categories  = new Dictionary<string, Category>(2500);
        private IDictionary<string, Currency>   currencies  = new Dictionary<string, Currency>();
        private IDictionary<string, Author>     authors     = new Dictionary<string, Author>(10000);
        private IDictionary<string, Language>   languages   = new Dictionary<string, Language>();
        private IDictionary<string, Publisher>  publishers  = new Dictionary<string, Publisher>(2000);
        private IDictionary<string, ParamName>  paramNames  = new Dictionary<string, ParamName>();
        private IDictionary<string, ParamValue> paramValues = new Dictionary<string, ParamValue>();
        
        private DataModule dataModule;


        public DataMover(DataModule dataModule)
        {
            this.dataModule = dataModule;
        }

        public void LoadData(DataReader reader)
        {
            try{ 
                LoadCurrencies(reader.GetXmlItems("currency"));
                LoadCategories(reader.GetXmlItems("category"));
                LoadBooks(reader.GetXmlItems("offer"));
            } catch (DbEntityValidationException dbEx) {
                foreach (var validationErrors in dbEx.EntityValidationErrors) {
                    foreach (var validationError in validationErrors.ValidationErrors) {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName,
                                               validationError.ErrorMessage);
                    }
                }
            }
        }


        private void LoadCurrencies(IEnumerable<XElement> items) {
            foreach (var item in items) {
                var id = item.Attribute("id").Value;
                var rate = (double)item.Attribute("rate");
                var currency = new Currency() {
                    Name = id,
                    Rate = rate
                };
                currencies[id] = currency;
            }
            dataModule.SaveChanges();
            Console.WriteLine("Commit currencies.");
        }


        private void LoadCategories(IEnumerable<XElement> items)
        {
            foreach (var item in items) {
                var id = item.Attribute("id").Value;
                var parentId = item.Attribute("parentId");
                var category = new Category() {
                                       Name = item.Value,
                                       Parent = parentId==null? null : categories[parentId.Value]
                                   };
                categories[id] = category;
            }
            dataModule.SaveChanges();
            Console.WriteLine("Commit categories.");
        }

        private void LoadBooks(IEnumerable<XElement> items) {
            int i = 0;
            foreach (var item in items) {
                var book = new Book() {
                    Author = item.GetEntity("author", authors),
                    Barcode = item.GetStringValue("barcode"),
                    Currency = item.GetEntity("currencyId", currencies),
                    Description = item.GetStringValue("description"),
                    Language = item.GetEntity("language", languages),
                    Name = item.GetStringValue("name"),
                    PictureUrl = item.GetStringValue("picture"),
                    Price = item.GetValue<double>("price"),
                    Publisher = item.GetEntity("publisher", publishers),
                    ReleaseYear = item.GetValue<int>("year"),
                    Url = item.GetStringValue("url"),
                    Categories = new List<Category>(item.Elements("categoryId").Select(x=>categories[x.Value]))
                };
                dataModule.Books.Add(book);
                
                if (++i%100 != 0) continue;
                dataModule.SaveChanges();
                Console.WriteLine("Commit books: {0}", i);
            }
            dataModule.SaveChanges();
        }



    }
}
