﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OzonDb
{

    public class DataModule : DbContext {
        public DbSet<Book>       Books        { get; set; }
        public DbSet<Category>   Categories   { get; set; }
        public DbSet<Currency>   Currencies   { get; set; }
        public DbSet<Author>     Authors      { get; set; }
        public DbSet<Language>   Languages    { get; set; }
        public DbSet<Publisher>  Publishers   { get; set; }
        public DbSet<ParamName>  ParamNames   { get; set; }
        public DbSet<ParamValue> ParamValues  { get; set; }
        public DbSet<Parameter>  Parameters   { get; set; }

        public DataModule(string name) : base(name) { }
    }

    public class Category : INamedEntity {
        [Key]            public int      Id     { get; set; }
        [MaxLength(80)]  public string   Name   { get; set; }
                         public Category Parent { get; set; }
    }

    public class Currency : INamedEntity {
        [Key]           public int    Id   { get; set; }
        [MaxLength(3)]  public string Name { get; set; }
                        public double Rate { get; set; }
    }

    public class Author : INamedEntity {
        [Key]            public int    Id   { get; set; }
        [MaxLength(202)] public string Name { get; set; }
    }

    public class Language : INamedEntity {
        [Key]           public int    Id   { get; set; }
        [MaxLength(20)] public string Name { get; set; }
    }

    public class Publisher : INamedEntity {
        [Key]            public int    Id   { get; set; }
        [MaxLength(203)] public string Name { get; set; }
    }

    public class ParamName : INamedEntity {
        [Key]            public int    Id   { get; set; }
        [MaxLength(100)] public string Name { get; set; }
    }

    public class ParamValue : INamedEntity {
        [Key]            public int    Id   { get; set; }
        [MaxLength(100)] public string Name { get; set; }
    }

    public class Parameter {
        [Key]           public int        Id    { get; set; }
                        public ParamName  Name  { get; set; }
                        public ParamValue Value { get; set; }
    }

    public class Book : INamedEntity {
        [Key]            public int             Id          { get; set; }
        [MaxLength(100)] public string          Url         { get; set; }
                         public Currency        Currency    { get; set; }
                         public double          Price       { get; set; }
                         public string          PictureUrl  { get; set; }
        [MaxLength(250)] public string          Name        { get; set; }
                         public Author          Author      { get; set; }
                         public Language        Language    { get; set; }
                         public Publisher       Publisher   { get; set; }
                         public int             ReleaseYear { get; set; }
        [MaxLength(2000)] public string         Description { get; set; }
        [MaxLength(13)]  public string          Barcode     { get; set; }
                         public List<Category>  Categories  { get; set; }
                         public List<Parameter> Parameters  { get; set; }
    }
}
